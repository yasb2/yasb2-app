# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

package Blog;
use Dancer2;
use File::Find;
use Try::Tiny;
use Blogger::Blogger;
use 5.018;

our $VERSION = '0.1';

# Define additional globals (Because I'm lazy)
our $website = "https://yasb2.blog";
our $title = "Yet another Sysadmin Blog 2 (YaSB2)";
our $sm_handle = "YaSB2";
our $max_articles = 10;
our $public_dir = "public";
our $xml_dir = join("/", $public_dir, "xml");
our $article_dir = join("/", $xml_dir, "articles");
our $comment_file = join("/", $xml_dir, "comments.xml");
our $about_file = join("/", $xml_dir, "about.xml");
our %templates = ( "article_page" => "article.tt",
		   "home_page" => "index.tt",
		   "archive_page" => "archive.tt",
		   "about_page" => "about.tt" );

# hooks
hook before_template_render => sub {
    my $tokens = shift;
    $tokens->{uri_base} = request->base->path;
};

# helper functions
sub get_all_articles {
    my @articles;
    find(sub { push @articles, $File::Find::name if /\.xml$/ }, join("/", $article_dir));
    return @articles;
};

sub get_article {
    # verfies file exists, then returns file
    # else, returns 404
    my $article = shift;
    try {
	die "Doesn't Exist!" unless -e $article;
	return $article
    }catch{
	try {
	    die "Doesn't Exist!" unless -e join("/",
						$article_dir,
						$article . ".xml");
	    return join("/", $article_dir, $article . ".xml");
	}catch{
	    return 404;
	};
    };
};

# view functions
sub view_home_page {
    my @articles = map { Article->new( file => $_ ) } get_all_articles();
    my $blog = Blog::Home->new( articles => \@articles,
				sm_handle => $sm_handle,
				url => $website,
				comment_file => $comment_file ,
				title => $title );

    template $templates{home_page} => { 'blog' => $blog };
};

sub view_archive_page {
    my @articles = map { Article->new( file => $_ ) } (get_all_articles());
    my $blog = Blog::Archive->new( articles => \@articles,
				   sm_handle => $sm_handle,
				   url => $website,
				   comment_file => $comment_file,
				   title => $title );

    template $templates{archive_page} => { 'blog' => $blog };
};

sub view_article_page {
    my $date = route_parameters->get('date');
    try {
	die "Page not found" unless $date =~ m/^[0-9]{4}-[0-9]{2}-[0-9]{2}$/;
	die "Page not found" if (get_article($date) eq 404 );
    }catch{
	status 'not_found';
	return 'Page not found';
    };

    my $article = Article->new( file => get_article($date) );
    my $blog = Blog::Article->new( article => $article,
				   sm_handle => $sm_handle,
				   url => $website,
				   comment_file => $comment_file,
				   title => $title );

    template $templates{article_page} => { 'blog' => $blog };
};

sub view_about_page {
    my $article = Article->new( file => $about_file );
    my $blog = Blog::Article->new( article => $article,
				   sm_handle => $sm_handle,
				   url => $website,
				   comment_file => $comment_file,
				   title => $title );
    template 'about.tt' => { 'blog' => $blog };
};

# routes
get '/' => \&view_home_page;

get (join('/', $_,':date?') => \&view_article_page) foreach ("articles", "archive");

get ("/" . $_ => \&view_archive_page) foreach ("articles", "archive");

get '/about' => \&view_about_page;

1;

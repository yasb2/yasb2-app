# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

use strict;
use warnings;
use utf8;
use 5.018;

package SocialMediaButton {
    use Moose;
    use URI::Escape;

    has ['name', 'url', 'class', 'style'] => ( is => 'rw',
					       isa => 'Str');
    
    sub onpress {
	# Onpress a message to Social Media service
	...
    };
};

package TwitterButton {
    use Moose;
    use URI::Escape;

    extends 'SocialMediaButton';
    
    has '+name' => ( default => 'twitter' );

    has '+url' => ( default => 'https://twitter.com/intent/tweet' );

    has '+class' => ( default => 'fa fa-twitter-square' );

    has '+style' => ( default => 'color: #00aced' );

    has ['onpress_url', 'onpress_text', 'onpress_via']  => ( is => 'rw',
    							     isa => 'Str',
    							     required => 1 );

    sub onpress {
    	my $self = shift;

    	return $self->{url} . "?" . join("&",
					 "url="  . uri_escape($self->onpress_url),
					 "text=" . uri_escape($self->onpress_text),
					 "via="  . uri_escape($self->onpress_via));
    };
};

package FacebookButton {
    use Moose;
    use URI::Escape;

    extends 'SocialMediaButton';
    
    has '+name' => ( default => 'facebook' );

    has '+url' => ( default => 'https://facebook.com/sharer.php');

    has '+class' => ( default => 'fa fa-facebook-square');

    has '+style' => ( default => 'color: #3b5998' );

    has 'onpress_u' => ( is => 'ro',
			 isa => 'Str',
			 required => 1 );

    sub onpress {
	my $self = shift;

	return $self->{url} . "?" . join("&",
					 "u=" . uri_escape($self->{onpress_u}));
    };

};

package RedditButton {
    use Moose;
    use URI::Escape;

    extends 'SocialMediaButton';

    has '+name' => ( default => 'reddit' );

    has '+url' => ( default => 'http://www.reddit.com/submit');

    has '+class' => ( default => 'fa fa-reddit-square');

    has '+style' => ( default => 'color: #ff4500' );

    has ['onpress_url', 'onpress_title'] => ( is => 'rw',
					      isa => 'Str',
					      required => 1 );

    sub onpress {
	my $self = shift;

	return $self->{url} . "?" . join("&",
					 "url=" . uri_escape($self->onpress_url),
					 "title=" . uri_escape($self->onpress_title));
    };

};

package LinkedinButton {
    use Moose;
    use URI::Escape;

    extends 'SocialMediaButton';

    has '+name' => ( default => 'linkedin' );

    has '+url' => ( default => 'http://www.linkedin.com/shareArticle');

    has '+class' => ( default => 'fa fa-linkedin-square');

    has '+style' => ( default => 'color: #0077b5' );

    has ['onpress_url', 'onpress_title', 'onpress_summary'] => ( is => 'ro',
								 isa => 'Str',
								 required => 1 );

    sub onpress {
	my $self = shift;

	return $self->{url} . "?" . join("&",
					 "mini=" . "true",
					 "url=" . uri_escape($self->onpress_url),
					 "title=" . uri_escape($self->onpress_title),
					 "summary=" . uri_escape($self->onpress_summary));
    };

};

package Article {
    use Moose;
    use File::stat;
    use XML::LibXML;
    use POSIX qw/strftime/;

    # relative or absolute path to file
    has 'file' => ( is => 'rw',
		    isa => 'Str',
		    trigger => \&_init);

    has 'date' => ( is => 'ro',
		    isa => 'Str',
		    writer => '_set_date' );

    has 'timestamp' => ( is => 'ro',
			 isa => 'Str',
			 writer => '_set_timestamp' );

    has 'summary'  => ( is => 'ro',
			isa => 'Str',
			writer => '_set_summary' );

    has	 'title'  => ( is => 'ro',
		       isa => 'Str',
		       writer => '_set_title' );
    has	 'tree'  => ( is => 'ro',
		      writer => '_set_tree' );

    has	 'author'  => ( is => 'ro',
			isa => 'Str',
			writer => '_set_author' );

    has	 'sm_table'  => ( is => 'rw',
			  isa => 'ArrayRef' );

    has 'content' => ( is => 'ro',
		       isa => 'Ref',
		       writer => '_set_content' );

    sub _init {
	my ($self, $file) = @_;
	my $filename = (split("/", $file))[-1];
	$self->_set_date(
	    substr($filename, 0, rindex($filename, ".xml"))
	    );

	$self->_set_timestamp(
	    strftime("%F %H:%M:%S", localtime(stat($file)->mtime))
	    );
	$self->_set_tree(XML::LibXML->load_xml(location => $file));
	$self->_set_title($self->tree->findvalue('.//article/title'));
        $self->_set_author($self->tree->findvalue('.//article/author'));

	my @content;
	for my $p ($self->tree->findnodes('.//article/content/p')) {
	    if ( $p->getAttribute('type') ){
		if ( $p->getAttribute('type') eq 'code' ){
		    my %hash = ( p => $p->to_literal(), type => 'code' );
		    push(@content, \%hash );
	        };
	    }else{
		my %hash = ( p => $p->to_literal(), type => 'text' );
		push(@content, \%hash );
	    };
	};
	$self->_set_content(\@content);
	$self->_set_summary(substr($self->{content}[0]->{p}, 0, 134) . " ...");
    };

};

package Comments {
    use Moose::Role;
    use XML::LibXML;
    
    has 'comment_file' => ( is => 'rw',
			    trigger => \&_init);

    has 'comments' => ( is => 'ro',
			isa => 'ArrayRef',
			writer => '_set_comments' );

    sub get_random_comment {
	my $self = shift;
	my @range = ( 0..scalar(@{ $self->{comments} })-1 );
	return $self->{comments}[rand(@range)];
    };

    sub _init{
	my ($self, $file) = @_;
	my $tree = XML::LibXML->load_xml(location => $file);
	my @comments;
	foreach my $comment_obj ($tree->findnodes('.//comments/comment')) {
	    my %hash = ( text => $comment_obj->findvalue('./text'),
			 code => $comment_obj->findvalue('./code') );
	    push(@comments, \%hash );
	};
	$self->_set_comments(\@comments);
    };
};

package Blog {
    use Moose;
    use URI::Escape;

    has ['url', 'sm_handle', 'title']  => ( is => 'rw',
					    isa => 'Str',
					    required => 1);

    with 'Comments';

    sub _set_article_sm_buttons {
	my $self = shift;
	my @articles = @_;
	for my $article (@articles) {
	    my @sm_buttons;
	    push @sm_buttons, TwitterButton->new( onpress_url => $self->url,
						  onpress_text => $article->title,
						  onpress_via => $self->sm_handle );
	    push @sm_buttons, FacebookButton->new( onpress_u => $self->url );
	    push @sm_buttons, RedditButton->new( onpress_url => $self->url,
						 onpress_title => $article->title );
	    push @sm_buttons, LinkedinButton->new( onpress_url => $self->url,
						   onpress_title => $article->title,
						   onpress_summary => $article->summary );
	    $article->sm_table(\@sm_buttons);
	};
    };
};

package Blog::Home {
    use Moose;

    extends 'Blog';
    has 'articles' => ( is => 'rw',
			isa => 'ArrayRef',
			trigger => sub { my $self = shift;
					 my $articles = shift;
					 $self->_set_article_sm_buttons(@$articles) },
			required => 1);

    sub get_last_articles {
    	my $self = shift;
    	my $num = shift // scalar(@{ $self->{articles} });
    	my @articles = sort { $b->file cmp $a->file } @{ $self->{articles} };
    	splice @articles, $num, scalar(@articles)-1;
    	return @articles;
    };
};

package Blog::Archive {
    use Moose;
    
    extends 'Blog::Home';
};

package Blog::Article {
    use Moose;

    extends 'Blog';

    has 'article' => ( is => 'rw',
		       isa => 'Article',
		       trigger => sub { my ($self, $article) = @_;
					$self->_set_article_sm_buttons($article) },
		       required => 1);
};

1;

#!/usr/bin/env perl

use strict;
use warnings;
use FindBin;
use lib "$FindBin::Bin/../lib";


# use this block if you don't need middleware, and only have a single target Dancer app to run here
use Blog;

Blog->to_app;

use Plack::Builder;

builder {
    enable 'Deflater';
    Blog->to_app;
}



=begin comment
# use this block if you want to include middleware such as Plack::Middleware::Deflater

use Blog;
use Plack::Builder;

builder {
    enable 'Deflater';
    Blog->to_app;
}

=end comment

=cut

=begin comment
# use this block if you want to include middleware such as Plack::Middleware::Deflater

use Blog;
use Blog_admin;

builder {
    mount '/'      => Blog->to_app;
    mount '/admin'      => Blog_admin->to_app;
}

=end comment

=cut

